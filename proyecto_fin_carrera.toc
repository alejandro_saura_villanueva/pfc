\contentsline {chapter}{Resumen}{V}{chapter*.3}
\contentsline {chapter}{Abstract}{VII}{chapter*.6}
\contentsline {chapter}{Agradecimientos}{IX}{chapter*.9}
\contentsline {chapter}{Contents}{XI}{section*.10}
\contentsline {chapter}{List of Figures}{XVII}{section*.12}
\contentsline {chapter}{List of Tables}{XIX}{section*.14}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.16}
\contentsline {section}{\numberline {1.1}Context}{3}{section.17}
\contentsline {section}{\numberline {1.2}Master thesis goals}{4}{section.18}
\contentsline {section}{\numberline {1.3}Structure of this Master Thesis}{5}{section.19}
\contentsline {chapter}{\numberline {2}Enabling Technologies}{7}{chapter.20}
\contentsline {section}{\numberline {2.1}Linked Data}{9}{section.21}
\contentsline {subsection}{\numberline {2.1.1}RDF}{9}{subsection.23}
\contentsline {subsection}{\numberline {2.1.2}SPARQL}{10}{subsection.46}
\contentsline {subsection}{\numberline {2.1.3}Geo Linked Data}{11}{subsection.59}
\contentsline {section}{\numberline {2.2}Web Components}{12}{section.74}
\contentsline {subsection}{\numberline {2.2.1}Custom HTML Elements}{12}{subsection.75}
\contentsline {subsection}{\numberline {2.2.2}HTML Imports}{12}{subsection.76}
\contentsline {subsection}{\numberline {2.2.3}Templates}{12}{subsection.77}
\contentsline {subsection}{\numberline {2.2.4}Shadow DOM}{13}{subsection.78}
\contentsline {section}{\numberline {2.3}Polymer}{13}{section.79}
\contentsline {section}{\numberline {2.4}Client-side web technologies}{14}{section.81}
\contentsline {subsection}{\numberline {2.4.1}Bootstrap}{14}{subsection.82}
\contentsline {subsection}{\numberline {2.4.2}Leaflet}{17}{subsection.92}
\contentsline {subsection}{\numberline {2.4.3}Crossfilter}{20}{subsection.128}
\contentsline {subsection}{\numberline {2.4.4}Dc.js}{20}{subsection.130}
\contentsline {section}{\numberline {2.5}Server-side web technologies}{21}{section.133}
\contentsline {subsection}{\numberline {2.5.1}MongoDB}{21}{subsection.134}
\contentsline {subsection}{\numberline {2.5.2}Fuseki}{22}{subsection.140}
\contentsline {section}{\numberline {2.6}Sefarad 1.0}{23}{section.142}
\contentsline {section}{\numberline {2.7}Summary}{24}{section.146}
\contentsline {chapter}{\numberline {3}Sefarad 2.0}{25}{chapter.147}
\contentsline {section}{\numberline {3.1}Introduction}{27}{section.148}
\contentsline {section}{\numberline {3.2}Architecture}{27}{section.149}
\contentsline {subsection}{\numberline {3.2.1}Dashboard Architecture}{30}{subsection.151}
\contentsline {subsubsection}{\numberline {3.2.1.1}Main module}{30}{subsubsection.152}
\contentsline {subsubsection}{\numberline {3.2.1.2}Data pre-processor module}{33}{subsubsection.235}
\contentsline {subsubsection}{\numberline {3.2.1.3}Web components}{34}{subsubsection.264}
\contentsline {subsubsection}{\numberline {3.2.1.4}Crossfilter-Dc.js}{38}{subsubsection.339}
\contentsline {subsection}{\numberline {3.2.2}Queries pipeline Architecture}{40}{subsection.379}
\contentsline {section}{\numberline {3.3}Interaction model}{47}{section.559}
\contentsline {subsection}{\numberline {3.3.1}Defining a data source}{47}{subsection.560}
\contentsline {subsection}{\numberline {3.3.2}Writing a query}{48}{subsection.562}
\contentsline {subsection}{\numberline {3.3.3}Visualising and editing a query}{49}{subsection.564}
\contentsline {section}{\numberline {3.4}Widgets}{52}{section.568}
\contentsline {subsection}{\numberline {3.4.1}Common implementation}{52}{subsection.569}
\contentsline {subsection}{\numberline {3.4.2}Specific implementation}{54}{subsection.592}
\contentsline {subsubsection}{\numberline {3.4.2.1}Faceted Search}{54}{subsubsection.593}
\contentsline {subsubsection}{\numberline {3.4.2.2}Results Table}{57}{subsubsection.659}
\contentsline {subsubsection}{\numberline {3.4.2.3}Pie Chart}{59}{subsubsection.685}
\contentsline {subsubsection}{\numberline {3.4.2.4}Bar Chart}{61}{subsubsection.711}
\contentsline {subsubsection}{\numberline {3.4.2.5}Bubble Chart}{62}{subsubsection.745}
\contentsline {subsubsection}{\numberline {3.4.2.6}Number Chart}{65}{subsubsection.802}
\contentsline {subsubsection}{\numberline {3.4.2.7}Reviews (Custom Widget example)}{69}{subsubsection.883}
\contentsline {subsubsection}{\numberline {3.4.2.8}Map}{71}{subsubsection.925}
\contentsline {section}{\numberline {3.5}Summary}{75}{section.994}
\contentsline {chapter}{\numberline {4}Case Study}{77}{chapter.995}
\contentsline {section}{\numberline {4.1}Introduction}{79}{section.996}
\contentsline {section}{\numberline {4.2}Slovak Demo: Smart Open data}{80}{section.997}
\contentsline {subsection}{\numberline {4.2.1}Origin}{80}{subsection.998}
\contentsline {subsection}{\numberline {4.2.2}Structure and pre-process}{86}{subsection.1030}
\contentsline {subsection}{\numberline {4.2.3}Analysis Design}{89}{subsection.1087}
\contentsline {subsection}{\numberline {4.2.4}Conclusions}{92}{subsection.1092}
\contentsline {section}{\numberline {4.3}Restaurants Demo}{92}{section.1093}
\contentsline {subsection}{\numberline {4.3.1}Origin}{92}{subsection.1094}
\contentsline {subsection}{\numberline {4.3.2}Structure and pre-process}{94}{subsection.1131}
\contentsline {subsection}{\numberline {4.3.3}Analysis Design}{97}{subsection.1198}
\contentsline {subsection}{\numberline {4.3.4}Conclusions}{100}{subsection.1203}
\contentsline {section}{\numberline {4.4}Tourpedia Demo}{100}{section.1204}
\contentsline {subsection}{\numberline {4.4.1}Origin}{100}{subsection.1205}
\contentsline {subsection}{\numberline {4.4.2}Structure and pre-process}{101}{subsection.1206}
\contentsline {subsection}{\numberline {4.4.3}Analysis Design}{103}{subsection.1223}
\contentsline {subsection}{\numberline {4.4.4}Conclusions}{104}{subsection.1227}
\contentsline {section}{\numberline {4.5}Summary}{104}{section.1228}
\contentsline {chapter}{\numberline {5}Evaluation}{105}{chapter.1229}
\contentsline {section}{\numberline {5.1}Introduction}{107}{section.1230}
\contentsline {section}{\numberline {5.2}Sefarad 1.0}{108}{section.1231}
\contentsline {subsection}{\numberline {5.2.1}Description}{108}{subsection.1232}
\contentsline {subsection}{\numberline {5.2.2}Benchmark}{111}{subsection.1235}
\contentsline {subsection}{\numberline {5.2.3}Analysis}{111}{subsection.1237}
\contentsline {section}{\numberline {5.3}Custom multi-query prototype}{112}{section.1238}
\contentsline {subsection}{\numberline {5.3.1}Description}{112}{subsection.1239}
\contentsline {subsection}{\numberline {5.3.2}Benchmark}{114}{subsection.1242}
\contentsline {subsection}{\numberline {5.3.3}Analysis}{114}{subsection.1244}
\contentsline {section}{\numberline {5.4}Bootstrap prototype}{115}{section.1245}
\contentsline {subsection}{\numberline {5.4.1}Description}{115}{subsection.1247}
\contentsline {subsection}{\numberline {5.4.2}Benchmark}{117}{subsection.1250}
\contentsline {subsection}{\numberline {5.4.3}Analysis}{117}{subsection.1252}
\contentsline {section}{\numberline {5.5}Crossfilter.js + dc.js prototype}{118}{section.1253}
\contentsline {subsection}{\numberline {5.5.1}Description}{118}{subsection.1254}
\contentsline {subsection}{\numberline {5.5.2}Benchmark}{119}{subsection.1257}
\contentsline {subsection}{\numberline {5.5.3}Analysis}{119}{subsection.1259}
\contentsline {section}{\numberline {5.6}Final Prototype: Web Components}{120}{section.1260}
\contentsline {subsection}{\numberline {5.6.1}Description}{120}{subsection.1261}
\contentsline {subsection}{\numberline {5.6.2}Benchmark}{120}{subsection.1262}
\contentsline {subsection}{\numberline {5.6.3}Analysis}{121}{subsection.1264}
\contentsline {section}{\numberline {5.7}Prototype comparison}{122}{section.1265}
\contentsline {section}{\numberline {5.8}Summary}{122}{section.1267}
\contentsline {chapter}{\numberline {6}Conclusions and future lines}{123}{chapter.1268}
\contentsline {section}{\numberline {6.1}Achieved goals}{125}{section.1269}
\contentsline {section}{\numberline {6.2}Conclusions}{126}{section.1270}
\contentsline {section}{\numberline {6.3}Future work}{127}{section.1272}
\contentsline {chapter}{\numberline {A}How to build a dashboard}{129}{appendix.1275}
\contentsline {section}{\numberline {A.1}Design}{131}{section.1276}
\contentsline {section}{\numberline {A.2}Widget Definition Example}{132}{section.1290}
\contentsline {section}{\numberline {A.3}Pre-processor}{134}{section.1310}
\contentsline {chapter}{Bibliography}{135}{section*.1312}
