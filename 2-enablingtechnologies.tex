


\chapter{Enabling Technologies}
\label{chap:enabling_technologies}
\begin{chapterintro}
This chapter introduces which technologies have made possible this project. First of all we must introduce Linked Data, RDF, SPARQL and GeoLinked Data~\cite{geolinkeddata1, geolinkeddata2}. Then we'll move on presenting all technologies that enable us to build a semantic front end with a data filtering and rendering system.
\end{chapterintro}

\cleardoublepage

\section{Linked Data}

Linked data is an attempt to describe entities, its properties and relationship with the objective of making them easily treated by machines. Linked Data has been recently suggested as one of the best alternatives for creating these shared information spaces \cite{geolinkeddata3}. It describes a method of publishing structured and related data so that it can be interlinked and become more useful, which results in the Semantic Web\footnote{http://www.w3.org/standards/semanticweb/} (also called Web of Data).

This master thesis aims to create a web application that enables a non-technical user to use the Linked Data Web. We will provide a framework where querying Linked Data and Geo Linked Data and visualizing interactively the results through dashboards will be possible.

\subsection{RDF}

Resource Description Framework (RDF) uses URIs to name the relationship between things as well as the two ends of the link (this is usually referred to as a “triple”). Using this simple model, it allows structured and semi-structured data to be mixed, exposed, and shared across different applications. 

Below, a sample RDF/XML file is shown, listing a table with two records and three fields:

\begin{lstlisting}[caption=RDF/XML document example, captionpos=b,language=rdf,basicstyle=\tiny ,frame=single]

<RDF:RDF xmlns:RDF="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
         xmlns:ANIMALS="http://www.some-fictitious-zoo.com/rdf#">

  <RDF:Seq about="http://www.some-fictitious-zoo.com/all-animals">
    <RDF:li>
       <RDF:Description about="http://www.some-fictitious-zoo.com/mammals/lion">
         <ANIMALS:name>Lion</ANIMALS:name>
         <ANIMALS:species>Panthera leo</ANIMALS:species>
         <ANIMALS:class>Mammal</ANIMALS:class>
       </RDF:Description>
    </RDF:li>    
    <RDF:li>
       <RDF:Description about="http://www.some-fictitious-zoo.com/mammals/hippopotamus">
         <ANIMALS:name>Hippopotamus</ANIMALS:name>
         <ANIMALS:species>Hippopotamus amphibius</ANIMALS:species>
         <ANIMALS:class>Mammal</ANIMALS:class>
       </RDF:Description>
    </RDF:li>
  </RDF:Seq>
</RDF:RDF>
\end{lstlisting}

\newpage

Each RDF:Description tag describes a single record. Within each record, three fields are described, name, species and class. Each of three fields have been given a namespace of ANIMALS, the URL of which has been declared on the RDF tag, where the semantic schema is defined.

The Linked Data paradigm hides the complexity of conceptual databases, maintaining them internal to the data providers and offering an interface where the user only has to know the semantics occurring in the data, the types that can conform subject-predicate expressions as triples in RDF form. This focus developers on specifying and sharing vocabularies describing their data instead of granting access to complex distributed databases. 

%\begin{acronym}
%\acro{RDF}{Resource Description Framework}
%\end{acronym}

\subsection{SPARQL}

Linked Data can be queried using SPARQL\footnote{http://www.w3.org/TR/rdf-sparql-query} (an acronym for SPARQL Protocol and RDF Query Language), a query language for RDF which became an official W3C Recommendation\footnote{http://www.w3.org/blog/SW/2008/01/15/}. The SPARQL query language consists of the syntax and semantics for asking and answering queries against RDF graphs and contains capabilities for querying by triple patterns, conjunctions, disjunctions, and optional patterns. Results of SPARQL queries can be presented in several different forms, such as JSON, RDF/XML, etc.

Here we present an example of a SPARQL query done against dbpedia, one of the largest endpoints available online:

\begin{lstlisting}[caption=SPARQL query example, captionpos=b, label=lst:sparql,
   basicstyle=\ttfamily,frame=single]
PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>
PREFIX dbpprop: <http://dbpedia.org/property/>
PREFIX dbres: <http://dbpedia.org/resource/>

SELECT ?y WHERE {
 ?y dbpedia-owl:binomialAuthority dbres:Johan_Christian_Fabricius.
 }

limit 10
\end{lstlisting}

\newpage

In this query, we import the dbpedia semantic schema and look for entities that match with our triples conditions. Please note how this is done through semantic statements instead of tables exploration.

\subsection{Geo Linked Data}

In the geospatial context, GeoLinked Data\footnote{http://linkedgeodata.org/} is an open initiative whose aim is to enrich the Semantic Web with geospatial data into the context of INSPIRE\footnote{http://inspire.ec.europa.eu/} \textit{(INfrastructure for SPatial InfoRmation in Europe)} Directive. This initiative focuses its efforts to collect, process and publish geographic information from different organizations around the world and providing the suitable tools for handing all the data. 

GeoSPARQL\footnote{http://www.opengeospatial.org/standards/geosparql} defines a vocabulary for representing geospatial data in RDF, and it defines an extension to the SPARQL query language for processing geospatial data. 

An example of these extra spatial relations is listed in ~\ref{lst:sparqlq}.

\begin{lstlisting}[caption=geoSPARQL query example, captionpos=b, label=lst:sparqlq,
   basicstyle=\ttfamily,frame=single]
PREFIX spatial:<http://jena.apache.org/spatial#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX geo:<http://www.w3.org/2003/01/geo/wgs84_pos#>
PREFIX gn:<http://www.geonames.org/ontology#>

Select * 
WHERE{
?object spatial:nearby(40.74 -73.989 1 'mi').
?object rdfs:label ?label
}LIMIT 10
\end{lstlisting}

\newpage

\section{Web Components}
\label{sec:webComponents}

Web Components \cite{webComponents} are a set of standards currently being produced by Google engineers as a W3C specification that allows for the creation of reusable widgets or components in web documents and web applications. The intention behind them is to bring component-based software engineering to the World Wide Web. The components model enables encapsulation and interoperability of individual HTML elements.

This idea comes from the union of four main standards: custom HTML elements, HTML imports, templates and shadow DOMs.

\subsection{Custom HTML Elements}

Custom Elements let the user define his own element types with custom tag names. JavaScript code is associated with the custom tags and uses them as an standard tag. That code gets executed each time the compiler reads that tag.

Standard DOM methods can be used on Custom Elements, as accessing their properties, attaching event listeners, and styling them using CSS as with any standard tag.

Thanks to custom tags the amount of code is reduced, internal details encapsulated, APIs per element type can be implemented, productivity is increased by reusing elements and advantage of inheritance is taken to develop new tags based on existing ones.

\subsection{HTML Imports}

HTML Imports let users include and reuse HTML documents in other HTML documents, as 'script' tags let include external Javascript in pages.
In particular, these imports include custom element definitions from external URLs. HTML imports use the import relation on a standard 'link' tag.

\subsection{Templates}

Templates define a new 'template' element which describes a standard DOM-based approach for client-side templating. Templates allow developers to declare fragments of markup which are parsed as HTML, go unused at page load, but can be instantiated later on at runtime.

\subsection{Shadow DOM}

There is a fundamental problem that makes widgets built out of HTML and JavaScript hard to use: the DOM tree inside a widget isn’t encapsulated from the rest of the page.
This lack of encapsulation means that the document stylesheet might accidentally be applied to parts inside the widget; JavaScript might accidentally modify parts inside the widget; IDs might overlap with IDs inside the widget; and so on.

Shadow DOM separates content from presentation therefore eliminating naming conflicts and improving code expression. It is internal to the element and hidden from the end-user.

\section{Polymer}

%More about Polymer, example
Polymer\footnote{https://www.polymer-project.org/0.5/} is an implementation o these four technologies in one elegant framework of constructing web-components.

Polymer makes it simple to create web components, declaratively. Custom elements are defined using our custom element,  'polymer-element', and can leverage Polymer’s special features. These features reduce boilerplate and make it even easier to build complex, web component-based applications:

\begin{itemize}



\item Two-way data binding: Data binding extends HTML and the DOM APIs to support a sensible separation between the UI (DOM) of an application and its underlying data (model). Updates to the model are reflected in the DOM and user input into the DOM is immediately assigned to the model.

\item Declarative event handling: Binding of events to methods in the component. It uses special on-event syntax to trigger this binding behavior.

\item Declarative inheritance: A Polymer element can extend another element by using the extends attribute. The parent’s properties and methods are inherited by the child element and data-bound.

\item Property observation: All properties on Polymer elements can be watched for changes by implementing a propertyNameChanged handler. When the value of a watched property changes, the appropriate change handler is automatically invoked.

\end{itemize}


\section{Client-side web technologies}

\subsection{Bootstrap}

Bootstrap\footnote{http://getbootstrap.com/} is the most popular HTML, CSS, and JavaScript framework for developing responsive web sites.
It features a multidevice preprocessor, based in Less\footnote{http://lesscss.org/} and Sass\footnote{http://sass-lang.com/libsass} that provides instant coherent style to the most common HTML elements and CSS components as tables, buttons, text fields, etc.

It offers a simple but powerful interface. All we have to do to integrate bootstrap styled elements into our website is add key class names to the supported elements and bootstrap will do the rest.

An example applied to HTML buttons and tables tables:

\begin{figure}[ht!]
	\centering
	\includegraphics[width=400px]{graphics/buttonstables.png}
	\caption{Basic HTML Table}
	\label{fig:basicTable}
\end{figure}

\newpage

Bootstrap offers flexibility and reusability, and that's the main reason why 
a lot of developers have been working into web site templates (free and commercial) at the top of the bootstrap functionality, adding their own extensions and producing an easy visual solution to the graphical user interface of a web application.

In the case of this project, we have explored some options and chosen AdminLTE\footnote{https://almsaeedstudio.com/}. 

\begin{figure}[ht!]
	\centering
	\includegraphics[width=400px]{graphics/adminLTE.png}
	\caption{adminLTE template}
	\label{fig:adminLTE}
\end{figure}

\newpage

This bootstrap template is free and open source, built on top of bootstrap 3, responsive, easy to customize and has an active community. But further than the visual style, this template offer us a set of features very interesting for our purposes in the data analysis.

It implements libraries as Flot.js\footnote{http://www.flotcharts.org/} and Morris.js\footnote{http://morrisjs.github.io/morris.js/}, two graph renderers that we could eventually inject in our dashboards with some works transforming them into Sefarad widgets (See widgets chapter).

\begin{figure}[ht!]
	\centering
	\includegraphics[width=400px]{graphics/flotChart.png}
	\caption{Premade Charts}
	\label{fig:adminLTE}
\end{figure}

It also provides some useful mechanisms as 404 error screen, log-in/log-out windows, lockscreens, etc.

It definitely provides a good graphical source for future development and it would be interesting to continue integrating its widgets as Sefarad's.

It presents some problems though at the time of using its functionality inside Polymer components. We will explore this problem in chapter \ref{chap:sefarad2}.

\newpage


\subsection{Leaflet}

Leaflet\footnote{http://leafletjs.com/} is a modern open-source JavaScript library for interactive maps. Leaflet is designed with simplicity, performance and usability in mind. It works efficiently across all major desktop and mobile platforms out of the box, taking advantage of HTML5 and CSS3 on modern browsers while still being accessible on older ones. It can be extended with a huge amount of plugins, has a beautiful, easy to use and well-documented API and a simple, readable source code. Weighing just about 33 KB of JS code, it has all the features most developers ever need for online maps.

It offers support to markers and pop-up binding, that we will use extensively in our demos.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=400px]{graphics/leafletMarker.png}
	\caption{Leaflet Marker and pop-up}
	\label{fig:leaflet}
\end{figure}

We chose this framework as a replacement of Open Layers 3 due to the features it provides for geoJSON polygon and multi-polygon rendering, with an automatic coordinate conversion between the base layer and the polygons definition. GeoJSON is becoming a very popular data format among many GIS technologies and services — it's simple, lightweight, straightforward, and Leaflet is quite good at handling it.

A GeoJSON object may represent a geometry, a feature, or a collection of features. GeoJSON supports the following geometry types: Point, LineString, Polygon, MultiPoint, MultiLineString, MultiPolygon, and GeometryCollection. Features in GeoJSON contain a geometry object and additional properties, and a feature collection represents a list of features.

\newpage

\begin{figure}[ht!]
	\centering
	\includegraphics[width=400px]{graphics/geoJSONlayer.png}
	\caption{Leaflet GeoJSON Layer}
	\label{fig:leaflet}
\end{figure}

In ~\ref{lst:geoJSONfea} we have listed an example of a simple GeoJSON feature.

\begin{lstlisting}[caption=geoJSON example, captionpos=bt, label={lst:geoJSONfea}, language=JavaScript, basicstyle=\small, frame=single]

var geojsonFeature = {
    "type": "Feature",
    "properties": {
        "name": "Coors Field",
        "amenity": "Baseball Stadium",
        "popupContent": "This is where the Rockies play!"
    },
    "geometry": {
        "type": "Point",
        "coordinates": [-104.99404, 39.75621]
    }
};

\end{lstlisting}

\newpage

GeoJSON objects are added to the map through a GeoJSON layer. 
The style option can be used to style features, even based on their properties as we can observe in the code block \ref{lst:styPro}.

\begin{lstlisting}[caption=Styling By Property Example, captionpos=b, label={lst:styPro}, language=JavaScript, basicstyle=\small, frame=single]
L.geoJson(geojsonFeature).addTo(map);
L.geoJson(states, {
    style: function(feature) {
        switch (feature.properties.party) {
            case 'Republican': return {color: "#ff0000"};
            case 'Democrat':   return {color: "#0000ff"};
        }
    }
}).addTo(map);
\end{lstlisting}

The onEachFeature option is a function that gets called on each feature before adding it to a GeoJSON layer. A common reason to use this option is to attach a popup to features when they are clicked, as we list in \ref{lst:popbind}.

\begin{lstlisting}[caption=Pop-up Binding, captionpos=b, label={lst:popbind}, language=JavaScript, basicstyle=\small, frame=single]
function onEachFeature(feature, layer) {
    // does this feature have a property named popupContent?
    if (feature.properties && feature.properties.popupContent) {
        layer.bindPopup(feature.properties.popupContent);
    }
}
\end{lstlisting}

\newpage

\subsection{Crossfilter}

Crossfilter\footnote{http://square.github.io/crossfilter/} is a JavaScript library for exploring large multivariate datasets in the browser.
It supports extremely fast (less than 30 milliseconds) interaction with coordinated views. Since most interactions only involve a single dimension, only small adjustments are made to the filter values, so incremental filtering is significantly faster than starting from scratch. Crossfilter uses sorted indexes to make this possible, dramatically increasing the performance of live histograms and top-K lists. 

\subsection{Dc.js}

dc.js\footnote{http://dc-js.github.io/dc.js/} is a javascript charting library with native crossfilter support that enables highly efficient exploration on large multi-dimensional dataset. It uses d3 engine to render charts in css friendly svg format. Charts rendered using dc.js are naturally data driven and reactive therefore providing instant feedback on user's interaction. dc.js provides us a great library of charts ready to use, as we illustrate in figure \ref{fig:dcex}.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=350px]{graphics/dc.png}
	\caption{dc.js Charts}
	\label{fig:dcex}
\end{figure}

\newpage

Besides, it offers a base chart that can be used to build a graphical interface on it. 
Base chart is an abstract functional object representing a basic dc chart object for all chart and widget implementations. Every function on the base chart are also inherited available on all all chart implementations extending base chart in dc library. We will use this one in a great number of widgets, and propose this template (after being integrated into Polymer) as the starting point at the task of constructing a custom widget for Sefarad-2.0.

Despite all these great capabilities, dc.js still has some problems, as the initialization of widgets at runtime. 
We will detail this problem in chapter \ref{chap:sefarad2}.

\section{Server-side web technologies}

\subsection{MongoDB}

MongoDB\footnote{https://www.mongodb.org/} is an open-source document-oriented NoSQL database distributed under the GNU Affero General Public License\footnote{http://www.gnu.org/licenses/agpl-3.0.html} and the Apache License\footnote{http://www.apache.org/licenses/LICENSE-2.0.html}. 

MongoDB is structured into collections instead of table-based relations. Those collections are a set of BSON (Binary JSON) documents containing a set of fields or key-value pairs: keys are string and values cans be of so many types (string, integer, float, timestamp, etc.). 

In MongoDB, information is stored in form of documents which doesn't have a predefined schema.
In place of that, these documents have a JSON array structure with multiple depth levels.

MongoDB is optimized for query operations. You can store as much information as you need in a document without first defining its structure, and this data will be able to be queried. In order to retrieve one o more documents, you may run your own query specifying some criteria or conditions. A query may support search by field, range or conditional statements such as the existence or not of a key. This makes the system highly scalable. 

This fits perfectly with our needs. We will use MongoDB to store user accounts, data-sources definitions, queries and their results in json or geo-json, output formats from SPARQL endpoints.

\newpage

Moreover, we can use mongoDB as a datasource itself, and support queries. In block code \ref{lst:onclcode} we show an example of a query pointed to a mongoDB.

\begin{lstlisting}[caption=Map Widget onClick Code, captionpos=b, label={lst:onclcode}, language=JavaScript, basicstyle=\small, frame=single]
{"$or": [{"year": {"$gt":1965,"$lt":2010}}, {"rating": {"$gt":6}}]}
\end{lstlisting}

This query selects all movies made between 1965 and 2010 or with a minimun rate of 6, expecting a response in JSON format.

MongoDB supports drivers for most common programming languages. Due to the fact that the structure of a document is similar to a JSON object and most of programming languages drivers support the management and conversion of JSON datatypes to language-specific structures, it is easy to communicate and manipulate the data. In the case of this project, we create a mongoDB and connect to it with the functions of the DART framework.

\subsection{Fuseki}

Fuseki \footnote{https://jena.apache.org} is a SPARQL server. It provides REST-style SPARQL HTTP Update, SPARQL Query, and SPARQL Update using the SPARQL protocol over HTTP.
We will use a Fuseki server for storing our own RDF files containing geoSPARQL data as a external tool for Sefarad datasets.

\newpage


\section{Sefarad 1.0}

Sefarad\cite{robertobermejo}\cite{ruben2014} is a web application developed to explore linked data by making SPARQL queries
to the chosen endpoint without writing more code, so it provides a semantic front-end to
Linked Open Data. It allows the user to configure his own dashboard with many different
widgets to visualize, explore and analyse graphically the different characteristics, attributes and relationships of the queried data. 

Sefarad is developed in HTML5\footnote{http://www.w3.org/TR/html5/} and follows a Model View-View Model (MVVM) pattern performed with the Knockout\footnote{http://knockoutjs.com/} framework. This JavaScript library allows us to create responsive and dynamic interfaces which automatically is updated when the data changes. The different parts of the UI are connected to the data model by declarative bindings.

Sefarad consists of two different tabs: dashboard and SPARQL Editor. The first tab allows the user to perform faceted search on the data accessed, so the users can explore a collection of information by applying multiple filters. In the SPARQL Editor tab we can write and preview queries to a defined endpoint.

\begin{figure}[ht]
	\centering
	\includegraphics[width=300px]{graphics/Sefarad1.png}
	\caption{Sefarad 1.0}
	\label{fig:dc}
\end{figure}

\pagebreak

We will use this framework as a guide and reference to develop our own framework implementing the same functionality and growth capabilities.

\section{Summary}

We have seen in this chapter a couple of distinct technologies that we use in our final prototype:

\begin{itemize}

\item Linked Data: We use the concepts of linked data philosophy querying RDF datasets through SPARQL queries. On top of that basis we use geoSPARQL directives to take advantages of Geo Linked Data location features.
 
\item Web Components: we make from web components our basic widget structure, taking into consideration its four principles.
 
\item Polymer: It is the web components concrete framework that we have. It provides us an easy way of implementing web components in our web.

\item Mongo DB: offers us a repository where we will store user data as data sources and queries definitions, as well as this kind of databases will serve us as data sources.

\item Bootstrap - AdminLTE: We use bootstrap to style our web and take advantage of its style encapsulation. In this context, on top of bootstrap, we choose AdminLTE as a template to construct our graphical interface.

\item Leaflet: We use Leaflet to implement a map widget with geoJSON compatibility and an interesting set of features.

\item Dc.js - Crossfilter: We use Crossfilter as the filtering core of our framework and, on top of that, Dc.js makes charts for us and manages them tracking their filters and updating their graphics.

\item Sefarad 1.0: It is our predecessor and, therefore, our reference in matter of features and interaction design.

\end{itemize}

