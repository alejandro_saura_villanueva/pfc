\chapter{Case Study}
\label{chap:casestudy}
\begin{chapterintro}
In this chapter we are going to describe a selected use cases by explaining the running of all the tools involved and its purpose and responses. Thank to these use cases, we will show the overall performance of the application and all the main functions available to the user.
\end{chapterintro}

\cleardoublepage

\section{Introduction}

To show the different features and tools Sefarad provides, we have experienced three different case studies,  each of which helps us to show different aspects of the application. These three case studies are:

\begin{itemize}
\item \textbf{European Universities.} In this study, we run a SPARQL query to the DBpedia endpoint querying about all universities in European countries. In this case study we:
	\begin{itemize}
	\item Select a SPARQL endpoint.
	\item Edit and run our own SPARQL query.
	\item Display the retrieved data in a graphical table.
	\item Point the geographical position of the results into an \textit{Open Street Map}\footnote{http://www.openstreetmap.org/} by using \textit{Openalayers.js} framework.
	\item Add some filtering widgets to test faceted search and keyword search
	\item Edit and run some SPARQL queries in the \textit{dashboard} tab for managing some static values. 
	\end{itemize}
\item \textbf{Restaurants and Districts in Madrid.} In this study, we run a SPARQL query to our local dataset deployed in a Fuseki installation. We query about restaurants in Madrid with some piece of information such as price, ranking, food type, etcetera. The result data will contain a RDF triple containing GeoJSON information about the city district in which it is located, allowing us to represent polygons in \textit{Openlayers}. In this case study we:
	\begin{itemize}
	\item Query a Fuseki dataset.
	\item Display the accordion layout to include many facets for filtering.
	\item Display GeoJSON polygons in an OpenLayers map. 
	\end{itemize}
\item \textbf{Slovakian parcels.} In this case study we will work with the dataset provided by the Slovak Environmental Agency (SEA\footnote{http://www.sazp.sk/}) about harmonised protected sites dataset according to INSPIRE\footnote{http://inspire.ec.europa.eu/} Data Specification on Protected Sites – Guidelines through WFS service interface. In this case we will also work with GeoJSON data as in the case before, so we will test about the same features, but the purpose of this case is to test the performance of the application with a more professional and standardized dataset. In other words, to show a professional GIS application of Sefarad.
\end{itemize}

\section{European universities}
\label{sec:universities}
In this case study we will query, index, classify, filter and display Linked Data relating to European Universities available in DBpedia. This is a very simple case of study that will help us to try the main features of Sefarad, including all those relative to query a SPARQL endpoint, manage the data with different filtering methods and display the final results in some graphical and geographical widgets. The following sub-sections will guide you through each of the steps with the help of listings, graphics and screenshots. 

\subsection{Query and retrieve the data: SPARQL}

At this point, we select an external endpoint: \textit{DBpedia\footnote{http://dbpedia.org/}}. We run the following SPARQL query to retrieve all the universities in Europe with its name and location (country, city and geographic coordinates).

\hspace{0.5cm}

\begin{lstlisting}[language=rdf, caption=European universities SPARQL, captionpos=b, label=lst:universitiesquery,
   basicstyle=\small, frame=single]
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\begin{lstlisting}[language=rdf, caption=Slovakia results JSON example, captionpos=b, label=lst:slovakiajsonresult,
   basicstyle=\small, frame=single]
{
  "res": {
    "type": "uri",
    "value": "http://geop.sazp.sk/id/ProtectedSite/ProtectedSitesSK/SKNATS942"
  },
  "fGeom": {
    "type": "uri",
    "value": "http://geop.sazp.sk/id/ProtectedSite/ProtectedSitesSK/geometry/SKNATS942"
  },
  "fWKT": {
    "datatype": "http://www.opengis.net/ont/sf#wktLiteral",
    "type": "typed-literal",
    "value": "MULTIPOLYGON(((17.65642811769707 48.1686865811456,..., 17.65642811769707 48.1686865811456)))"
  },
  "spc": {
    "type": "literal",
    "value": "natureConservation ecological environment"
  },
  "lfd": {
    "type": "literal",
    "value": ""
  },
  "lfdoc": {
    "type": "literal",
    "value": ""
  },
  "inspire": {
    "type": "uri",
    "value": "http://geop.sazp.sk/id/ProtectedSite/ProtectedSitesSK/inspireId/SKNATS942"
  },
  "namespace": {
    "type": "literal",
    "value": "SK:GOV:MOE:SEA:PS"
  },
  "localId": {
    "type": "literal",
    "value": "SK:GOV:MOE:SEA:PS"
  },
  "siteDesignation": {
    "type": "uri",
    "value": "http://geop.sazp.sk/id/ProtectedSite/ProtectedSitesSK/siteDesignation/SKNATS942"
  },
  "percentageUnderDesignation": {
    "type": "literal",
    "value": ""
  },
  "designation": {
    "type": "literal",
    "value": "wildernessArea"
  },
  "designationScheme": {
    "type": "literal",
    "value": "IUCN"
  }
}
\end{lstlisting}
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dc: <http://purl.org/dc/terms/>
PREFIX dbpedia: <http://dbpedia.org/>
PREFIX dbpedia2: <http://dbpedia.org/ontology/>
PREFIX geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>

SELECT ?university ?country ?city ?latitude ?longitude 

WHERE {
	?universityResource rdf:type dbpedia2:University ;
	dbpedia2:country ?countryResource ; 
	dbpedia2:city ?cityResource ; 
	rdfs:label ?university ; 
	geo:lat ?latitude ; 	geo:long ?longitude . 
	?countryResource rdfs:label ?country ;
	dc:subject dbpedia:Countries_in_Europe.
	?cityResource rdfs:label ?city 

	FILTER ( lang(?university) = 'en' )
}
\end{lstlisting}

An example piece of the information retrieved is shown below.

\hspace{1cm}

\begin{lstlisting}[language=rdf, caption=Universities results JSON example, captionpos=b, label=lst:jsonresult,
   basicstyle=\small, frame=single]
{
	"university": {
		"type": "literal",
		"xml:lang": "en",
		"value": "University of Barcelona"
	},
	"city": {
		"type": "literal",
		"xml:lang": "en",
		"value": "Barcelona"
	},
	"country": {
		"type": "literal",
		"xml:lang": "en",
		"value": "Spain"
	},
	"latitude": {
		"type": "typed-literal",
		"datatype": "http://www.w3.org/2001/XMLSchema#float",
		"value": "41.3867"
	},
	"longitude": {
		"type": "typed-literal",
		"datatype": "http://www.w3.org/2001/XMLSchema#float",
		"value": "2.16389"
	}
}
\end{lstlisting}

We retrieve the response data in JSON format. As you could suppose, each one of the results contains five keys: university, country, city, latitude, longitude; as we indicate in the \textit{SELECT} sentence of the query. Once the information is retrieved, the application stores and indexes it, obtaining the different facets for future faceted search work. 

\clearpage

\subsection{Showing the data: results table}
The main widget available in Sefarad for exploring and ordering the final results is the \textit{Results Table} widget. You can add it from the \textit{'Add new widget'} section. Once you have done it, the widget automatically recognises the different facets of the results and draws them into a table like the one above (Figure[\ref{fig:universitiestable}]).

\begin{figure}[ht!]
	\centering
	\includegraphics[width=400px]{img/universitiestable.png}
	\caption{Results Widget with universities}
	\label{fig:universitiestable}
\end{figure}

As we work with large amounts of data, we realized the need to do this widget as much configurable as possible, so that the user can select how many items to display per section, which columns to show or hide, etcetera. Furthermore, we included in the results table a search box that the user can use to search within the filtered results, which is an extra functionality apart from filtering technologies. All those configuration options can be shown in the figure above. 

Moreover, if you have included the URI of the different  resources in the SPARQL query, you can browse the results by clicking on them in the results table. For example, by clicking on \textit{American University of Rome}, the following web page is opened.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=400px]{img/rome.png}
	\caption{American University of Rome DBpedia webpage}
	\label{fig:universitiestable}
\end{figure}

\subsection{Geographic representation: Openlayers map}
The main widget available in Sefarad for representing geospatial information is the \textit{Openlayers map} widget. In this case, since we only have coordinates information (latitude and longitude) we can only represent the universities' position points in the map. In the image below, we have filtered the results by country (Spain and Italy) for a better appreciation.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=400px]{img/universitiesmap.png}
	\caption{Universities in Spain and Italy}
	\label{fig:universitiesmap}
\end{figure}

\subsection{Filtering technologies}
The last feature we will experience in this case study is both filtering technologies: faceted and keyword. To use keyword filtering, we must enter our keywords into the search box included at the right top of the application. As we write, the application will give us options that match what we have introduced, as shown in the image below. 

\begin{figure}[ht!]
	\centering
	\includegraphics[width=400px]{img/universitieskeyword.png}
	\caption{Keyword search example}
	\label{fig:universitieskeyword}
\end{figure}

To use faceted search, we must add some widgets that show the different possible values for the selected facet. After adding this widget for the \textit{country} facet, the final layout display for this \textit{Universities Demo} is shown in Figure [\ref{fig:universitieslayout}].

\begin{figure}[ht!]
	\centering
	\includegraphics[width=400px]{img/universitiesdemo.png}
	\caption{Universities Demo Layout}
	\label{fig:universitieslayout}
\end{figure}

\section{Restaurants and Districts in Madrid}
\label{sec:restaurants}

In this study, we run a SPARQL query to our local dataset deployed in a Fuseki installation. We query about restaurants in Madrid\footnote{http://www.madrid.org/nomecalles/}\footnote{http://www.yelp.com/madrid} with some piece of information such as price, ranking, food type, etcetera. Result data contains a RDF triple containing GeoJSON information about the district in which it is located, allowing us to represent polygons in \textit{Openlayers}.

\subsection{Download and process the information}
In this case, we download the data\footnote{http://www.madrid.org/nomecalles/}\footnote{http://www.yelp.com/madrid} and process it in order to upload it to our Fuseki database. The following graph shows this process in a clear way. 

\vspace{1cm}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=400px]{graphics/smartopendataArchitecture.png}
	\caption{Processing restaurants data}
	\label{fig:universitieslayout}
\end{figure}

\subsection{SPARQL and Fuseki}
In this case, we will not query a SPARQL endpoint of an online dataset. We will store the information in RDF format on a local server (Fuseki) and we will query it from Sefarad. We have created two new databases in Fuseki: \textit{districts} and \textit{restaurants}. 

\begin{lstlisting}[language=rdf, caption=Restaurants results JSON example, captionpos=tb, label=lst:restjsonresult,
   basicstyle=\small, frame=single]
"s": {
	"type": "uri",
	"value": "http://smartopendata.gsi.dit.upm.es/rdf/gu/Features/773372"
},
"fGeom": {
	"type": "uri",
	"value": "http://smartopendata.gsi.dit.upm.es/rdf/gu/Geometries/90478c001031d2ab9e9c199257ecbbb2724edb77"
},
"fWKT": {
	"datatype": "http://www.opengis.net/ont/sf#wktLiteral",
	"type": "typed-literal",
	"value": "MULTIPOLYGON (((444197.329 4477208.2759, ...  , 444197.329 4477208.2759)))"
},
"geocodigo": {
	"type": "literal",
	"value": "7904"
},
"desbdt": {
	"type": "literal",
	"value": "Salamanca"
},
"dbpediaLink": {
	"type": "uri",
	"value": "http://dbpedia.org/resource/Salamanca_(Madrid)"
},
"d": {
	"type": "uri",
	"value": "http://sefarad.gsi.dit.upm.es/rdf/gp/restaurants/casa-julian-madrid-2"
},
"p": {
	"type": "uri",
	"value": "http://sefarad.gsi.dit.upm.es/rdf/gp/district"
},
"o": {
	"type": "literal",
	"value": " Salamanca "
},
"price": {
	"type": "literal",
	"value": ""
},
"foodtype": {
	"type": "literal",
	"value": " Spanish "
},
"stars": {
	"type": "literal",
	"value": "4.5"
},
"reservations": {
	"type": "literal",
	"value": " Takes Reservations No "
},
"takeout": {
	"type": "literal",
	"value": "No"
},
"lat": {
	"type": "literal",
	"value": "40.429118500000001"
},
"long": {
	"type": "literal",
	"value": "-3.6858382999999999"
}
\end{lstlisting}

As you can see in the image above, which shows the piece of information related to one restaurant within the retrieved data, each restaurant contains geographical information as a \textit{MULTIPOLYGON} value, so we can draw it in a map.

\clearpage

The SPARQL query for this case study is shown below.

\vspace{0.25cm}

\begin{lstlisting}[language=rdf, caption=European universities SPARQL, captionpos=b, label=lst:universitiesquery,
   basicstyle=\small, frame=single]
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
PREFIX gnis: <http://smartopendata.gsi.dit.upm.es/rdf/gnis/>
PREFIX gu: <http://smartopendata.gsi.dit.upm.es/rdf/gu/> 
PREFIX drf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX dcterms: <http://purl.org/dc/terms/>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX dbpedia-owl: <http://dbpedia.org/property/>
prefix text: <http://jena.apache.org/text#>
PREFIX gp: <http://sefarad.gsi.dit.upm.es/rdf/gp/> 

SELECT * WHERE {

	SERVICE <http://localhost:3030/districts/query> {
        ?s geo:hasGeometry ?fGeom .
        ?fGeom geo:asWKT ?fWKT .
        ?s gu:GEOCODIGO ?geocodigo  .
        ?s gu:DESBDT ?desbdt .  
        ?s owl:sameAs ?dbpediaLink .  
	}

	SERVICE <http://localhost:3030/restaurants/query> {
		?d ?p ?o 
		FILTER(REGEX(?o, ?desbdt))
	}

	SERVICE <http://localhost:3030/restaurants/query> {
		?d gp:price ?price .
		?d gp:foodtype ?foodtype .
		?d gp:stars ?stars .
	}
}
\end{lstlisting}

We have run a query that allows us to retrieve information from various data types (districts and restaurants). In addition, we have combined the information available in our dataset with information available in DBpedia, so what gives us access to more facets of the data.

\subsection{Faceted search}
As in the case of European universities, we will experience faceted search. However, in this case we have a greater number of facets, allowing us to observe the results when combining it. After adding a \textit{Tag Cloud} widget for each of the facets we want to filter by, we have the next application layout.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=300px]{img/alltagcloud.png}
	\caption{Linear layout}
	\label{fig:linearlayoutrestaurants}
\end{figure}

As you can see in the image above, when the number of facets becomes higher and higher, it becomes an impractical interface, because we can not show all the widgets in the window at the same time, and if we filter by a facet whose widget is lower, we must 'remove' the display map, so we can not see how the results are updated. To solve this problem, we developed another type of layout: the accordion layout [\ref{subsubsec:accordion}]. Applying respective layout, the previous screen becomes as follows.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=300px]{img/accordionrestaurants.png}
	\caption{Accordion layout}
	\label{fig:accordionlayoutrestaurants}
\end{figure}


\subsection{Openlayers and GeoJSON}
In this case study, we will not only represent points on a map, but also polygons. The geographical data about districts stored in Fuseki is in GeoSPARQL format, as you can see in[\ref{lst:restjsonresult}]. 

We can easily represent it into an Openlayers map by using \textit{OpenLayers.Format.GeoJSON\footnote{http://dev.openlayers.org/docs/files/OpenLayers/Format/GeoJSON-js.html}} class. But since that Openlayers requires a specific format GeoJSON for proper representation, and our districts' information is in GeoSPARQL format, we were required to develop the \textit{Geo Proxy [\ref{sec:geoproxy}]}. to convert GeoSPARQL to GeoJSON. Once the data is converted, we have a \textit{FeatureCollection} containing all  districts polygons in the respective features. The next figure shows an example of feature in GeoJSON.

\vspace{1cm}

\begin{lstlisting}[language=rdf, caption=Restaurants results JSON example, captionpos=b, label=lst:restjsonresult,
   basicstyle=\small, frame=single]
{
	"type": "Feature",
	"geometry": {
		"type": "MultiPolygon",
		"coordinates": [	[440414.61, 4473164.38], ..., [440414.61, 4473164.38]]
	},
	"properties": {
		"s": {},
		"fGeom": {},
		"fWKT": {},
		"geocodigo": {},
		"desbdt": {},
		"dbpediaLink": {},
		"price": {},
		"foodtype": {},
		"stars": {},
		"reservations": {},
		"takeout": {},
		"lat": {},
		"long": {}
	}
}
\end{lstlisting}

\section{Slovakian dataset}
\label{sec:slovakian}
In this case study we will use the application from a more professional standpoint. For this purpose, we will work with the dataset provided by the Slovak Environmental Agency (SEA\footnote{http://www.sazp.sk/}) about harmonised protected sites dataset according to INSPIRE\footnote{http://inspire.ec.europa.eu/} Data Specification on Protected Sites – Guidelines through WFS service interface. 

In this case we will also work with GeoJSON data as in the case before, so we will test about the same features, but the purpose of this case is to test the performance of the application with a more professional and standardized dataset. In other words, to show a professional GIS application of Sefarad.

\subsection{Protected sites dataset}
SEA provided source files for Geoserver workspaces related to slovak protected sites feature type. Source files can be downloaded from SEA website\footnote{http://inspire.geop.sazp.sk/geoserver/www/eenvplus/ps\_harmonisation.tgz}.

Protected sites data are stored in GIS database as simple features in several database tables. Tables slightly differ in structure, they contain some common and some different fields. The structure of database tables is application dependant and was designed in the past during applications development, different applications are used for different kind of protected sites. Upon discussion with domain expert mapping between individual protected sites tables and protected sites designation schemas was established.

Subsequently unifying view could be constructed, unioning several protected sites tables into one logical database view with common set of data fields. Set of common data fields was chosen according to INSPIRE Protected Site simple profile application scheme. Fields for designation scheme and designation scheme value (both mandatory in application schema) were added to view for easing later mapping. Figure \ref{fig:slovakiascheme} shows the data scheme.

\pagebreak

\begin{figure}[ht!]
	\centering
	\includegraphics[width=400px]{graphics/slovakiascheme.png}
	\caption{Protected sites tables and view}
	\label{fig:slovakiascheme}
\end{figure}

The final data have the following fields and information values, which will be used to test faceted browsing and geo filtering with ECQL\footnote{http://docs.geoserver.org/latest/en/user/filter/ecql\_reference.html}.

\vspace{0.25cm}

\begin{longtable}{|p{7cm}|p{4cm}|p{3cm}|}
\hline
\textbf{INSPIRE Designation} & \textbf{SK Designation} & \textbf{SK Legislation} \\ \hline
natura2000/siteOfCommunity OImportance & Uzemia europskeho vyznamu/ Sites of Community importance & Act of NC SR No. 543/2002 on Nature and Landscape Protection \\ \hline
natura2000/specialProtectionArea & Chranene vtacie uzemia/ Special protection areas & Act of NC SR No. 543/2002 on Nature and Landscape Protection \\ \hline
ramsar/ramsar & Ramsar/Ramsar sites & Act of NC SR No. 543/2002 on Nature and Landscape Protection  \\ \hline
UNESCOWorldHeritage/natural & Unesco/Unesco natural heritage sites & Act of NC SR No. 543/2002 on Nature and Landscape Protection  \\ \hline
UNESCOManAndBiosphereProgramme/ biosphereReserve & Biosfericke rezervacie/Biosphere reserves & Act of NC SR No. 543/2002 on Nature and Landscape Protection \\ \hline
\multirow{2}{*}{IUCN/nationalPark} & Velkoplošné chránené územia/Large scale protected ares Národný park/National park & Act of NC SR No. 543/2002 on Nature and Landscape Protection   \\ \cline{2-3}
 & Velkoplošné chránené územia/Large scale protected ares Chránené krajinné oblasti /Protected landscape area & Act of NC SR No. 543/2002 on Nature and Landscape Protection  \\ \hline
\multirow{4}{*}{IUCN/strictNatureReserve}  &  Maloplošné chránené územia (Small scale protected areas): Prírodná rezervácia/Nature reserve & Act of NC SR No. 543/2002 on Nature and Landscape Protection \\ \cline{2-3}
 & Maloplošné chránené územia (Small scale protected areas): Národná prírodná rezervácia/National nature reserve & Act of NC SR No. 543/2002 on Nature and Landscape Protection \\ \cline{2-3}
 & Maloplošné chránené územia(Small scale protected areas): Ochranné pásmo prírodnej rezervácie/Buffer zone of natural reserve & Act of NC SR No. 543/2002 on Nature and Landscape Protection \\ \cline{2-3}
 & Maloplošné chránené územia(Small scale protected areas): Ochranné pásmo národnej prírodnej rezervácie/Buffer zone of national nature reserve & Act of NC SR No. 543/2002 on Nature and Landscape Protection \\ \hline
\multirow{6}{*}{IUCN/ naturalMonument} & Maloplošné chránené územia(Small scale protected areas): Chránený krajinný prvok/Protected landscape element & Act of NC SR No. 543/2002 on Nature and Landscape Protection \\ \cline{2-3}
 & Maloplošné chránené územia(Small scale protected areas): Prírodná pamiatka/Natural monument & Act of NC SR No. 543/2002 on Nature and Landscape Protection \\ \cline{2-3}
 & Maloplošné chránené územia(Small scale protected areas): Národná prírodná pamiatka/National natural monument & Act of NC SR No. 543/2002 on Nature and Landscape Protection \\ \cline{2-3}
 & Maloplošné chránené územia(Small scale protected areas): Chránený areál/Protected site & Act of NC SR No. 543/2002 on Nature and Landscape Protection \\ \cline{2-3}
 & Maloplošné chránené územia(Small scale protected areas): Ochranné pásmo prírodnejpamiatky/Buffer zone of natural monument & Act of NC SR No. 543/2002 on Nature and Landscape Protection \\ \cline{2-3}
 & Maloplošné chránené územia(Small scale protected areas): Ochranné pásmo národnej prírodnej pamiatky/Buffer zone of national natural monument & Act of NC SR No. 543/2002 on Nature and Landscape Protection \\ \hline
\multirow{4}{*}{IUCN/ wildernessArea} & Maloplošné chránené územia(Small scale protected areas): Ochranné pásmo chráneného areálu/Buffer zone of protected site & Act of NC SR No. 543/2002 on Nature and Landscape Protection \\ \cline{2-3}
 & Chránené krajinné územie (Protected landscape area) & Act of NC SR No. 543/2002 on Nature and Landscape Protection \\ \hline
\caption{Data INSPIRE designation}
\label{tab:slovakiantable}
\end{longtable}

\pagebreak

\subsection{SPARQL Query and results data}
In this case, we also store the information into a local Fuseki database and we query it from Sefarad. Query for retrieving the corresponding information is shown below.

\vspace{1cm}

\begin{lstlisting}[language=rdf, caption=Slovakian Demo SPARQL, captionpos=b, label=lst:slovakiaquery,
   basicstyle=\small, frame=single]
PREFIX drf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX j.0: <http://inspire.jrc.ec.europa.eu/schemas/gn/3.0/> 
PREFIX j.1: <http://inspire.jrc.ec.europa.eu/schemas/ps/3.0/> 
PREFIX j.2: <http://inspire.jrc.ec.europa.eu/schemas/base/3.2/> 
PREFIX j.3: <http://www.opengis.net/ont/geosparql#> 
\newpage
SELECT * 
  WHERE { 
	SERVICE <http://localhost:3030/slovakia/query> { 
		?res j.3:hasGeometry ?fGeom . 
		?fGeom j.3:asWKT ?fWKT . 
		?res j.1:siteProtectionClassification ?spc  . 
		?res j.1:LegalFoundationDate ?lfd .   
		?res j.1:LegalFoundationDocument ?lfdoc .  
		?res j.1:inspireId ?inspire . 
		?inspire j.2:namespace ?namespace . 
		?inspire j.2:namespace ?localId . 
		?res j.1:siteDesignation ?siteDesignation .  
		?siteDesignation j.1:percentageUnderDesignation ?percentageUnderDesignation . 
		?siteDesignation j.1:designation ?designation . 
		?siteDesignation j.1:designationScheme ?designationScheme . 
	} 
}

LIMIT 10;        
\end{lstlisting}

\pagebreak

At this point, a piece of the information returned from the server is as follows. 

\vspace{1cm}

\begin{lstlisting}[language=rdf, caption=Slovakia results JSON example, captionpos=b, label=lst:slovakiajsonresult,
   basicstyle=\small, frame=single]
{
  "res": {
    "type": "uri",
    "value": "http://geop.sazp.sk/id/ProtectedSite/ProtectedSitesSK/SKNATS942"
  },
  "fGeom": {
    "type": "uri",
    "value": "http://geop.sazp.sk/id/ProtectedSite/ProtectedSitesSK/geometry/SKNATS942"
  },
  "fWKT": {
    "datatype": "http://www.opengis.net/ont/sf#wktLiteral",
    "type": "typed-literal",
    "value": "MULTIPOLYGON(((17.65642811769707 48.1686865811456,..., 17.65642811769707 48.1686865811456)))"
  },
  "spc": {
    "type": "literal",
    "value": "natureConservation ecological environment"
  },
  "lfd": {
    "type": "literal",
    "value": ""
  },
  "lfdoc": {
    "type": "literal",
    "value": ""
  },
  "inspire": {
    "type": "uri",
    "value": "http://geop.sazp.sk/id/ProtectedSite/ProtectedSitesSK/inspireId/SKNATS942"
  },
  "namespace": {
    "type": "literal",
    "value": "SK:GOV:MOE:SEA:PS"
  },
  "localId": {
    "type": "literal",
    "value": "SK:GOV:MOE:SEA:PS"
  },
  "siteDesignation": {
    "type": "uri",
    "value": "http://geop.sazp.sk/id/ProtectedSite/ProtectedSitesSK/siteDesignation/SKNATS942"
  },
  "percentageUnderDesignation": {
    "type": "literal",
    "value": ""
  },
  "designation": {
    "type": "literal",
    "value": "wildernessArea"
  },
  "designationScheme": {
    "type": "literal",
    "value": "IUCN"\begin{lstlisting}[language=rdf, caption=Slovakia results JSON example, captionpos=b, label=lst:slovakiajsonresult,
   basicstyle=\small, frame=single]
{
  "res": {
    "type": "uri",
    "value": "http://geop.sazp.sk/id/ProtectedSite/ProtectedSitesSK/SKNATS942"
  },
  "fGeom": {
    "type": "uri",
    "value": "http://geop.sazp.sk/id/ProtectedSite/ProtectedSitesSK/geometry/SKNATS942"
  },
  "fWKT": {
    "datatype": "http://www.opengis.net/ont/sf#wktLiteral",
    "type": "typed-literal",
    "value": "MULTIPOLYGON(((17.65642811769707 48.1686865811456,..., 17.65642811769707 48.1686865811456)))"
  },
  "spc": {
    "type": "literal",
    "value": "natureConservation ecological environment"
  },
  "lfd": {
    "type": "literal",
    "value": ""
  },
  "lfdoc": {
    "type": "literal",
    "value": ""
  },
  "inspire": {
    "type": "uri",
    "value": "http://geop.sazp.sk/id/ProtectedSite/ProtectedSitesSK/inspireId/SKNATS942"
  },
  "namespace": {
    "type": "literal",
    "value": "SK:GOV:MOE:SEA:PS"
  },
  "localId": {
    "type": "literal",
    "value": "SK:GOV:MOE:SEA:PS"
  },
  "siteDesignation": {
    "type": "uri",
    "value": "http://geop.sazp.sk/id/ProtectedSite/ProtectedSitesSK/siteDesignation/SKNATS942"
  },
  "percentageUnderDesignation": {
    "type": "literal",
    "value": ""
  },
  "designation": {
    "type": "literal",
    "value": "wildernessArea"
  },
  "designationScheme": {
    "type": "literal",
    "value": "IUCN"
  }
}
\end{lstlisting}
  }
}
\end{lstlisting}

As you can see, JSON results contain many geographical properties, following the data scheme explained in [\ref{tab:slovakiantable}].

Up to this point, the process is the same as that followed in the previous case study.

\subsection{OpenStreet Map: GeoJSON representation}
In this case study, we will use the features for polygons representation that Openlayers offers. First of all, we need to convert the retrieved geoSPARQL data to GeoJSON data by using the GeoProxy. After doing that, we will have a \code{FeatureCollection} containing one feature for each parcel in the dataset. An example of one of this features is shown in the next listing. 

\pagebreak

\begin{lstlisting}[language=rdf, caption=Slovakian feature example, captionpos=b, label=lst:slovakiafeature,
   basicstyle=\small, frame=single]
{
  "type": "Feature",
  "geometry": {
    "type": "MultiPolygon",
    "coordinates": [
      [
        [
          [17.65642811769707, 48.1686865811456],
          [...]
          [17.65642811769707, 48.1686865811456]
        ]
      ]
    ]
  },
  "properties": {
  }
}
\end{lstlisting}

Drawing it with Openlayers and OpenStreet Map, we get the following map result.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=400px]{img/map.png}
	\caption{Protected sites map}
	\label{fig:slovakiamap}
\end{figure}

\section{SmartOpenData parcels dataset}
\label{sec:geoserverdemo}
Finally, in this case study we will use a shapefiles dataset provided by \textit{Tragsa}\footnote{http://www.tragsa.es/} for the SmartOpenData project. The main purpose of this case study is to test Sefarad performance with GeoServer. Tragsa has provided us a new dataset about different kinds of parcels in Spain in DBF and shapefile format. The data scheme is shown in Figure \ref{fig:parcelsdatascheme}.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=390px]{img/parcelsscheme.png}
	\caption{Dataset scheme}
	\label{fig:parcelsdatascheme}
\end{figure}

\subsection{Parcels data scheme}
We have stored all the shapefiles in our local GeoServer installation. From there, we are able to see all the different parcels, see its feature types, preview them with Openlayers directly from GeoServer, test ECQL filtering, etcetera. 

For this case study, we have chosen the \code{td\_0307\_sigpac} parcels, whose feature types are detailed in Figure \ref{fig:featuretypes}.

\vspace{1cm}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=400px]{img/layertable.png}
	\caption{Feature Types Details}
	\label{fig:featuretypes}
\end{figure}

\subsection{GeoServer and Openlayers}
In this case, we do not need the GeoProxy to convert the retrieved data. We can query to GeoServer directly from Sefarad and represent the geographical data in an Openlayers map. For this case, the SPARQL query is shown in Listing \ref{lst:parcelsquery}. 

\vspace{1cm}

\begin{lstlisting}[language=rdf, caption=SMOData Dataset SPARQL query, captionpos=b, label=lst:parcelsquery,
   basicstyle=\small, frame=single]
PREFIX ns3:   <http://smartod.gsi.dit.upm.es/parcels0307/schema#>

SELECT ?shape_area ?parcel ?slope ?use WHERE { 
     ?s a ns3:Parcels . 
     ?s a ?itemtype ; 
     ns3:Shape_Area ?shape_area ; 
     ns3:PENDIENTE_ ?slope ; 
     ns3:USO_SIGPAC ?use ; 
     ns3:PARCELA ?parcel 
}
\end{lstlisting}

We must also indicate the graph of the requested data, which is:\\
\code{http://dataset1.smartopen.gsi.edu}.

After receiving the results, we can directly represent them without any conversion in an Openlayers map, as shown in Figure \ref{fig:smodmap}.

\vspace{1cm}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=400px]{img/smodmap.png}
	\caption{Openlayers map}
	\label{fig:smodmap}
\end{figure}

\subsection{Geo-filtering: ECQL}
As you can see in Figure \ref{fig:smodmap}, this widget includes a filtering box which allows us to test geofiltering with the retrieved data. We can include any ECQL query in this field and the geofiltering module will automatically update and rerun the query to receive the information that meets the new criteria. Figure \ref{fig:smod} shows the complete layout of this case study. 

\vspace{1cm}

\begin{figure}[ht!]
	\centering
	\includegraphics[width=400px]{img/smod.png}
	\caption{SmartOpenData layout}
	\label{fig:smod}
\end{figure}








